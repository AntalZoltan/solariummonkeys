SolariumMonkeys  business logic

Hogyan lesz a Lead -ből Closed Won?

Miután egy kereskedő megkapta a Lead-jeit, egyenként felveszi velük a kapcsolatot. 
A beszélgetés vagy a levelezés alapján felméri a valós igényeket, elkéri a cég adatokat, kapcsolattartó adatokat.

Ha az igény kiszolgálható és tényleges, akkor a begyűjtött adatok alapján a kereskedő új Account-ot és ahhoz tartozó új Contact -ot, illetve egy új Opportunity-t hoz létre, a Lead konvertálással.
Az Opportunity -nek több állapota is van, létrehozáskor automatikusan „Qualifying” státuszba kerül. 
Amennyiben további egyeztetés szükséges a Contact-tal, akkor felveszi vele a kapcsolatot, személyesen találkozhat vele, bemutathatja a szolárium gépeket, bemutathatja a bérleti és lízing konstrukciókat, szolgáltatásokat.

A kereskedő árajánlatot (Quote) készít. Az árajánlatra tételeket vesz fel (QuoteLineItem), amelyek tartalmazzák az adott termék vagy szolgáltatás részleteit (Név, Mennyiség, Diszkont). 

Amennyiben a kereskedő nem ad diszkont árat, akkor a Quote állapota automatikusan Approved-ra változik, és az Opportunity státusz átkerül „Proposal/Price Quote” állapotba.
Ha az adott Quote-hoz egy olyan QuoteLineItem is tartozik, amelynek a Discount mezője ki van töltve, akkor a program Quote státuszát automatikusan In Review-ra állítja. 
Ekkor a Quote StageName mezője WaitingForApproval állapotra vált, és egy trigger feladatot (Task) hoz létre a Sales Manager-nek. 

A Sales Manager a Task felületen a "Mark Complete" gombbal a Quote-ot Approved állapotba teheti, és ezzel egyidejűleg az Opportunity is átkerül „Proposal/Price Quote” állapotba.

Egy időzített osztály figyeli, hogy mennyi idő telt el az árajánlat küldés óta, azaz azóta, hogy az Opportunity "Proposal/Price Quote" állapotba került. 
Ha több mint 3 nap telt már el, és az Opportunity még nem került „Order” állapotba, akkor jelez a kereskedőnek, egy feladatot (Task) hoz létre, hogy keresse fel újra az ügyfelet.

Amikor az ügyfél elfogadja az árajánlatot és visszajelez mailben vagy telefonon, akkor a kereskedő az Opportunity-t „Order” állapotba teszi.
Abban az esetben, ha az Opportunity még „Proposal/Price Quote” állapotú, de az ügyfél több megkeresésre sem reagál, a kereskedő az Opportunity-t „ClosedLost” állapotba teheti.

A rendszer időzített osztály segítségével figyeli, hogy az Opportunity-hez tartozó számla fizetési határideje nem járt-e még le. 
A cég alapesetben 30 napos fizetési határidőt alkalmaz. 
Ha az Opportunity „Order” állapotba kerülés után az adott fizetési határidő leteltével sem kerül „Paid” állapotba, akkor ez az időzitett osztály létrehoz egy feladatot (Task) a kereskedőnek, aki felveszi az ügyféllel a kapcsolatot.

Amennyiben a számla kifizetésre került, akkor a kereskedő az Opportunity-t „Paid” állapotba teszi. 
Abban az esetben, ha az Opportunity még „Order” állapotú, de az ügyfél még a fizetési határidő leteltével sem fizette ki a számlát, és az ügyfél elérhetetlen, a kereskedő az Opportunity-t „ClosedLost” állapotba teheti.

A „Paid” állapotba került Opportunity-n egy trigger létrehoz egy Task -ot, ami utasítja a Deployment Manager-t, az adott darabszámú és tipusú szolárium kiszállítására és beüzemelésére. 

Egy trigger a Quote, az Account, és Contact adatok segítségével létrehoz egy Asset rekordot, ami tartalmazza a kiszállitott solárium gép adatait..
Az Asset rekordot a Deployment Manager szerkesztheti, adatokat hozzáadhat, státuszát változtathatja.

Egy trigger figyeli az Asset objektumot, és ha a státusza "Installed" akkor az Opportunity-t „Delivered” állapotba teszi.

A kereskedő a „Delivered” állapotú Opportunity-t lezárhatja, azaz „ClosedWon” állapotba teheti. 






Az üzleti logika megvalósitásához triggereket, trigger kezelő osztályokat, és időzitett osztályokat irtunk és használtunk.