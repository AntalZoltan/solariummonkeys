public class AddTaskToAnotherUser {
    public void addTaskForDelivery(String shipperId){
        //ide kell majd betenni a szállító Id-ját, most Thomas Jefferson a nyertes
        //String shipperId = '0055J0000016rPrQAI'; 
        List<Opportunity> paidOpportunities = [
            									SELECT Id, OwnerId 
            									FROM Opportunity 
            									WHERE StageName = 'Paid'
            									LIMIT 100
        										];
        for(Opportunity o: paidOpportunities) {
            Task t = new Task();
            t.OwnerId = shipperId; 
            t.Subject = 'Deliver the item to the customer!';
            t.Status = 'Open';
            t.Priority = 'Normal';
            t.WhatId = o.Id;
            insert t;
    	}
	}
    
    public void approveQuote(){
        
    }
}
