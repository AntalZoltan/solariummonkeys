/*
Task objektum Status mezője lehet Not Started, In Progress, Completed vagy Deferred. Aikor létrejön akkor Not Started.
Amikor létrejön egy Task objektum, akkor az tartalmaz egy Quote_For_Task__c custom lookup mezőt, ami az adott Quote Id-ja,
és egy Asset_For_Task__c custom lookup mezőt, ami az adott Asset Id-ja.
Fontos, hogy vagy a Quote-ra mutató Id-t, vagy az Asset-ra mutató Id-t tartalmazhatja, mindkettőt egyszerre nem.
Ebből különböztetjük meg, hogy Quote vagy Asset miatt jött létre a Task.

Használati esetek:
ApproveQuoteTask: (Ezt a Task-ot az OpportunityTriggerHandler hozza létre)
Ha az Opportunity Stage mezője WaitingForApprove értékű, a Task objektum Quote_For_Task__c custom lookup mezője nem üres, és a Task objektum Status mezője Completed, 
akkor a Quote objektum Status mezője Accepted lesz és az Opportunity Stage mezője Proposal/Price Quote értékbe vált.
Ha a Task objektum Status mezője Deferred, akkor a Quote Status objektum mezője Denied lesz.

CallClientForOrderTask: (Ezt a Task-ot egy időzitett osztály hozza létre)
Ha az Opportunity Stage mezője Approved értékű, és a Task objektum Status mezője Completed, akkor az Opportunity Stage mezője Order értékbe vált.
Ha az Opportunity Stage mezője Approved értékű, és a Task objektum Status mezője Deferred, akkor az Opportunity Stage mezője ClosedLost értékbe vált.
Task objektum Description mezője kitölthető, átmásoljuk az Opportunity Description mezőjébe.

CallClientForPaidTask: (Ezt a Task-ot egy időzitett osztály hozza létre)
Ha az Opportunity Stage mezője Order értékű, és a Task objektum Status mezője Completed, akkor az Opportunity Stage mezője Paid értékbe vált.
Ha az Opportunity Stage mezője Order értékű, és a Task objektum Status mezője Deferred, akkor az Opportunity Stage mezője ClosedLost értékbe vált.
Task objektum Description mezője kitölthető, átmásoljuk az Opportunity Description mezőjébe.

DeliverAssetTask: (Ezt a Task-ot az OpportunityTriggerHandler hozza létre)
Ha az Opportunity Stage mezője Paid értékű, a Task objektum Asset_For_Task__c custom lookup mezője nem üres, és a Task objektum Status mezője Completed, 
akkor az Opportunity Stage mezője Delivered értékbe vált.

GetBackAssetTask: (Ezt a Task-ot egy időzitett osztály hozza létre)
Ha az Opportunity Stage mezője Delivered vagy Closed Won értékű, a Task objektum Asset_For_Task__c custom lookup mezője nem üres, 
az aktuális dátum  egyezik a UsageEndDate-tel vagy nagyobb, és a Task objektum Status mezője Completed, 
akkor az Asset custom IsReturned__c mezője true értékbe vált.
*/
public class TaskTriggerHandler {


    public static void handleBeforeInsert(List<Task> triggerNew){
	
    }
    
    
    public static void handleBeforeUpdate(Map<Id, Task> triggerOldMap, List<Task> triggerNew){
		
    }
    
    
    public static void handleAfterInsert(List<Task> triggerNew){        

    }
    
    
    public static void handleAfterUpdate(Map<Id, Task> triggerOldMap, List<Task> triggerNew){
	  	Date payDeadline = System.today();
    	for(Task task : triggerNew) {
            if (task.Status == 'Deferred' && triggerOldMap.get(task.Id).Subject == 'Call the Client! Are they interested of the quote?'){
                List<Quote> activeQuotes = [
												SELECT Id, Name, LineItemCount, OwnerId, OpportunityId
												FROM Quote 
												WHERE LineItemCount > 0
											];
					for(Quote q: activeQuotes) {
						if (q.OpportunityId == task.WhatId){
						   q.Status = 'Denied';
							update q;
							List<Opportunity> opportunitiesWaitingForOrder = [
																	SELECT Id, OwnerId 
																	FROM Opportunity 
																	WHERE StageName = 'Proposal/Price Quote'
																	LIMIT 100
																];
							for(Opportunity o: opportunitiesWaitingForOrder) {
								if (o.Id == task.WhatId){
									o.StageName = 'Closed Lost';
									update o;
								}  
							}
						  }
					}
            	}
        		if (task.Status == 'Deferred' && triggerOldMap.get(task.Id).Subject == 'Bill is not paid, contact the costumer'){
                    List<Quote> activeQuotes = [
												SELECT Id, Name, LineItemCount, OwnerId, OpportunityId
												FROM Quote 
												WHERE LineItemCount > 0
											];
					for(Quote q: activeQuotes) {
						if (q.OpportunityId == task.WhatId){
						   q.Status = 'Denied';
							update q;
                    		List<Opportunity> notPaidOpportunities = [
                                                SELECT Id, CloseDate, OwnerId 
                                                FROM Opportunity 
                                                WHERE StageName = 'Order' AND CloseDate < :payDeadline 
                                                LIMIT 100
                                                    ];
                    		for(Opportunity o: notPaidOpportunities) {
                        		if (o.Id == task.WhatId){
                            		o.StageName = 'Closed Lost';
                            		update o;
                        		}		  
                            }
                        }
                	}
                }
            if (task.Status == 'Completed' && triggerOldMap.get(task.Id).Subject == 'Bill is not paid, contact the costumer'){
                    List<Opportunity> notPaidOpportunities = [
                                                SELECT Id, CloseDate, OwnerId 
                                                FROM Opportunity 
                                                WHERE StageName = 'Order' AND CloseDate < :payDeadline 
                                                LIMIT 100
                                                    ];
                    for(Opportunity o: notPaidOpportunities) {
                        if (o.Id == task.WhatId){
                            o.StageName = 'Paid';
                            update o;
                        }  
                	}
            	} 
            	if (task.Status == 'Completed' && triggerOldMap.get(task.Id).Subject == 'Deliver the item to the customer!'){
					List<Opportunity> opportunitiesToDeliver = [
															SELECT Id, OwnerId 
															FROM Opportunity 
															WHERE StageName = 'Paid'
															LIMIT 100
														];
					for(Opportunity o: opportunitiesToDeliver) {
						if (o.Id == task.WhatId){
							o.StageName = 'Delivered';
							update o;
						}		
					}
				} 
            	if (task.Status == 'Completed' && triggerOldMap.get(task.Id).Subject == 'Call the Client! Are they interested of the quote?'){
					List<Quote> quotesWithLineItem = [
												SELECT Id, Name, LineItemCount, OwnerId, OpportunityId
												FROM Quote 
												WHERE LineItemCount > 0
											];
					for(Quote q: quotesWithLineItem) {
						if (q.OpportunityId == task.WhatId){
						   q.Status = 'Accepted';
							update q;
							List<Opportunity> opportunitiesWaitingForOrder = [
																	SELECT Id, OwnerId 
																	FROM Opportunity 
																	WHERE StageName = 'Proposal/Price Quote'
																	LIMIT 100
																];
							for(Opportunity o: opportunitiesWaitingForOrder) {
								if (o.Id == task.WhatId){
									o.StageName = 'Order';
									update o;
								}  
							}
						  }
					}
            	} 
            	if (task.Status == 'Completed' && triggerOldMap.get(task.Id).Subject == 'Deliver the Asset from the Customer to the factory'){
                	List<Asset> assetsEnded = [
                                        SELECT Id, Name, UsageEndDate, AccountId 
                                        FROM Asset 
                                        WHERE UsageEndDate = :System.today()
                                    ];
            	}
        }
    }
    
    
    public static void handleAfterDelete(List<Task> triggerOld){

    }

 
    
}
