global class ScheduledExpiredQuotesCallClient implements Schedulable{
	global void execute(SchedulableContext ex){
         List<Quote> quotesWithQuoteLineItem = [
            					SELECT Id, Name, LineItemCount, Created_Date__c, ExpirationDate, OwnerId, OpportunityId
            					FROM Quote 
            					WHERE LineItemCount > 0 AND Created_Date__c != null
        						];
        if(quotesWithQuoteLineItem.size() > 0){
             for(Quote q: quotesWithQuoteLineItem) {
                 if(q.Created_Date__c.daysBetween(q.ExpirationDate) > 3){
                    Task CallClientForOrder = new Task();
                    CallClientForOrder.OwnerId = q.OwnerId;
                    CallClientForOrder.Subject = 'Call the Client! Are they interested of the quote?';
                    CallClientForOrder.Status = 'Open';
                    CallClientForOrder.Priority = 'Normal';
                    CallClientForOrder.WhatId = q.OpportunityId;
                    CallClientForOrder.Quote_For_Task__c = q.Id;
                    insert CallClientForOrder;
                 }
        	}
        }
    }
}
