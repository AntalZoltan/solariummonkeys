trigger TaskTrigger on Task (before insert, before update, after insert, after update, after delete) {
    
    if(Trigger.isBefore){
        if(Trigger.isInsert){
            TaskTriggerHandler.handleBeforeInsert(Trigger.New);
        }else if(Trigger.isUpdate){
            TaskTriggerHandler.handleBeforeUpdate(Trigger.oldMap, Trigger.New);
        }        
    }else if(Trigger.isAfter){
        if(Trigger.isInsert){
            TaskTriggerHandler.handleAfterInsert(Trigger.New);
        }else if(Trigger.isUpdate){
            TaskTriggerHandler.handleAfterUpdate(Trigger.oldMap, Trigger.New);
        }else if(Trigger.isDelete){
            TaskTriggerHandler.handleAfterDelete(Trigger.old);
        }       
    }

}