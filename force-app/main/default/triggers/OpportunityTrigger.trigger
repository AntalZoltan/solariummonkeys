trigger OpportunityTrigger on Opportunity (before insert, before update, after insert, after update, after delete) {
    
    if(Trigger.isBefore){
        if(Trigger.isInsert){
            OpportunityTriggerHandler.handleBeforeInsert(Trigger.New);
        }else if(Trigger.isUpdate){
            OpportunityTriggerHandler.handleBeforeUpdate(Trigger.oldMap, Trigger.New);
        }        
    }else if(Trigger.isAfter){
        if(Trigger.isInsert){
            OpportunityTriggerHandler.handleAfterInsert(Trigger.New);
        }else if(Trigger.isUpdate){
            OpportunityTriggerHandler.handleAfterUpdate(Trigger.oldMap, Trigger.New);
        }else if(Trigger.isDelete){
            OpportunityTriggerHandler.handleAfterDelete(Trigger.old);
        }       
    }
}